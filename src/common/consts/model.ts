interface IDatasetsDoughnutChart {
    data: string[] | number[];
    backgroundColor: string[];
    hoverBackgroundColor: string[];
}

export interface IDataDoughnutChartJson {
    labels: string[];
    datasets: IDatasetsDoughnutChart[];
}

export interface IDataLineChart {
    year: string;
    yearData: string[];
}

export interface ILineChart {
    labels: string[];
    data: IDataLineChart[];
}

export interface IdataLineChartPopupState {
    title: string;
    data: ILineChart;
}