import React from "react";
import ToggleShortcut from "./ToggleShortcut";
import { connect } from "react-redux";
import profile from '../../../assets/icon-images/profile.png';
import * as NavigationActions from '../NavigationActions'
import { bindActionCreators } from "redux";

const LoginInfo = props => {
  return (
    <div className="login-info">
      <span>
        <ToggleShortcut>
          <img src={profile} alt="user name" className="online" style={{paddingLeft:"3px"}} />
          <span>user name</span>
          <i className="fa fa-angle-down" />
        </ToggleShortcut>
      </span>
    </div>
  );
}

export default connect(
  store => store.navigation,
  dispatch => bindActionCreators(NavigationActions, dispatch)
)(LoginInfo);
