import React from "react";

export default class Forgot extends React.Component {
  onClick = e => {
    e.preventDefault();
  };

  render() {
    return (
      <div id="extr-page">
        <header id="header" className="animated fadeInDown auth-header">
          <div id="logo-group">
            <a href="#/">
              <span id="logo">
                {" "}
                <img src="assets/img/logo-white.png" alt="SmartAdmin" className="img-auth-logo"/>{" "}
              </span>
            </a>
          </div>

          <span id="extr-page-header-space">
            {" "}
            <span className="hidden-mobile hiddex-xs">
              Need an account?
            </span>{" "}
            <a href="#/register" className="btn btn-danger">
              Create account
            </a>{" "}
          </span>
        </header>
        <div id="main" role="main" className="animated fadeInDown">
          <div id="content" className="container">
            <div className="msbit-flex-center">
              <div className="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                <div className="well no-padding">
                  <form
                    action="#/login"
                    id="login-form"
                    className="smart-form client-form"
                  >
                    <header>Forgot Password</header>

                    <fieldset>
                      <section>
                        <label className="label">
                          Enter your email address
                        </label>
                        <label className="input">
                          {" "}
                          <i className="icon-append fa fa-envelope" />
                          <input type="email" name="email" />
                          <b className="tooltip tooltip-top-right">
                            <i className="fa fa-envelope txt-color-teal" />{" "}
                            Please enter email address for password reset
                          </b>
                        </label>
                      </section>
                      <section>
                        <span className="timeline-seperator text-center text-primary">
                          {" "}
                          <span className="font-sm">OR</span>
                        </span>
                      </section>
                      <section>
                        <label className="label">Your Username</label>
                        <label className="input">
                          {" "}
                          <i className="icon-append fa fa-user" />
                          <input type="text" name="username" />
                          <b className="tooltip tooltip-top-right">
                            <i className="fa fa-user txt-color-teal" /> Enter
                            your username
                          </b>{" "}
                        </label>
                        <div className="note">
                          <a href="#/login">I remembered my password!</a>
                        </div>
                      </section>
                    </fieldset>
                    <footer>
                      <a
                        href="#/"
                        className="btn btn-primary"
                      >
                        <i className="fa fa-refresh" /> Reset Password
                      </a>
                    </footer>
                  </form>
                </div>

                <h5 className="text-center"> - Or sign in using -</h5>

                <ul className="list-inline text-center">
                  <li>
                    <a href="#/"
                      onClick={this.onClick}
                      className="btn btn-primary btn-circle"
                    >
                      <i className="fa fa-facebook" />
                    </a>
                  </li>
                  <li>
                    <a href="#/"
                      onClick={this.onClick}
                      className="btn btn-info btn-circle"
                    >
                      <i className="fa fa-twitter" />
                    </a>
                  </li>
                  <li>
                    <a href="#/"
                      onClick={this.onClick}
                      className="btn btn-warning btn-circle"
                    >
                      <i className="fa fa-linkedin" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
