import React, { useState } from "react";
import { BigBreadcrumbs, JarvisWidget, Stats, WidgetGrid } from "../../../common";
import { addAlertMessage, addConfirmMessageQ, reloadDatatable } from "../../../common/consts/functions";
import DataTableComponent from "../../data-table/dataTableComponent";
import { statsLayoutData } from "./jsons/statsLayoutArray";
import "./internationalizationDataTableExamplePage.scss";
import AddForm from "./componenets/addForm/addForm";import ModalPage from "../modal-page-example/modalPageExample";
 "./componenets/addForm/addForm";


const InternationalizationDataTableExamplePage: React.FC = () => {
  const [widgetId, setwidgetId] = useState<number | string>(0);

  const deleteWidgetConfirm = async (widgetId: number | string) => {
    addConfirmMessageQ("Are you sure you want to delete this widget?");
    let yesButtonFromConfirmAlert = $('#divSmallBoxes .textoFoto button#yesConfirmDel');
    // when the 'yes' is click add function delete
    console.log("widgetId: ", widgetId);
    yesButtonFromConfirmAlert.on('click', () => { deleteWidget(widgetId) });
  }

  const deleteWidget = async (widgetId: number | string) => {
    //the request to delete user by his id, if success alert success and reload(update) the table:
    let url = 'http://localhost:3000/internationalization/'+ widgetId;
    let resp = await fetch(url, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    });
    reloadDatatable();
    addAlertMessage("Success", `user is deleted`, "success");
  }

  return (
    <div>
      <div className="msbit-crumb-state">
        <BigBreadcrumbs
          items={["Examples", "DataTable"]}
          icon={"fa"}
          className=""
        />
        <Stats statsData={statsLayoutData} />
      </div>
      <div className="datatable-responsive list-table">
        <DataTableComponent
          amountPerPage={5}
          jsonDataTableUrl={"http://localhost:3000/internationalization"}
          columns={["id", "page", "language"]}
          thTitle={["ID", "page", "language"]}
          tableBordered={false}
          toggleBtnWidjet={true}
          title={"Users"}
          buttonName={"Add Widget"}
          setId={setwidgetId}
          deleteItem={deleteWidgetConfirm}
          editBtn={true}
          deleteBtn={true}
        />
      </div>
    </div>
  );
}
export default InternationalizationDataTableExamplePage;