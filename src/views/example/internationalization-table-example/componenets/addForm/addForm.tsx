import React, { useState } from 'react';
import BootstrapValidator from '../../../../../templates/forms/validation/BootstrapValidator';
import { FormData, FormDataProps } from '../../interface/formData.interface';
import WidgetJarvisWidget from '../WidgetJarvisWidget/WidgetJarvisWidget';
import './addForm.scss';

const validatorOptions = {
    container: "#messages",
    feedbackIcons: {
        valid: "glyphicon glyphicon-ok",
        invalid: "glyphicon glyphicon-remove",
        validating: "glyphicon glyphicon-refresh"
    },
    fields: {
        page: {
            validators: {
                notEmpty: {
                    message: "The page is required and cannot be empty"
                },
                stringLength: {
                    min: 2,
                    message: "The page must be higher than 2 characters long"
                }
            }
        }
    },
};




const AddForm: React.FC<FormDataProps> = (props: FormDataProps) => {
    const [formData, setFormData] = useState<FormData>(props.data || {
        page: '',
        language: []
    });

    const handleChange = event => {
        setFormData({
            ...formData,

            [event.target.name]: event.target.value,
        });
    }

    const handleChangeLanguage = (event, languageIndex) => {

        let languages = formData.language.map((language, i) => {
            if (i === languageIndex) {
                return {
                    ...language,
                    [event.target.name]: event.target.value
                }
            } else {
                return language;
            }
        })


        setFormData({
            ...formData,
            language: languages,
        })


    }
    const addLanguageButtonsLabel = (event, index) => {
        const newLabel = {
            label: "",
        }

        let languages = formData.language;
        languages.map((language, i) => {


            if (i === index) {
                if (!language.button) {
                    language.button = []
                } else {

                    language.button.push(newLabel);
                }

            }
        })
        setFormData({ ...formData, language: languages })


    }
    const handleButtonLabelChange = (event, languageIndex, buttonIndex) => {

        let languages = [...formData.language]
        languages[languageIndex].button[buttonIndex] = { label: event.target.value }


        setFormData({ ...formData, language: languages })
    }




    const onSubmit = (e) => {
        e.preventDefault();
        let url = 'http://localhost:3000/internationalization'
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)

        });
    }

    const addLanguage = () => {
        const newLangue = {
            code: "",
            direction: "",
            title: "",
            header: "",
            body: "",
            footer: "",
            button: []
        }

        let newFormData = {
            ...formData,
            language: [...formData.language, newLangue]
        }

        { console.log({ newFormData }); }
        setFormData(newFormData)
    }

    return <BootstrapValidator options={validatorOptions}>
            <form
                id="addForm"
                className="form-horizontal"
                onSubmit={onSubmit}
            >
                <fieldset>
                    <div className="form-group">
                        <label className="col-md-3 control-label">Page</label>
                        <div className="col-md-6">
                            <input type="text" className="form-control" value={formData.page} name="page" onChange={handleChange} />
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <div className="form-group">
                        <div className="col-md-3"></div>
                        <div className="col-md-6">
                            <button
                                className="dt-button btn btn-info"
                                tabIndex={0} aria-controls="data-table"
                                title="Add"
                                id="add-language"
                                type="button"
                                onClick={addLanguage}
                            >
                                <span>Add Language</span>
                            </button>
                        </div>
                    </div>
                </fieldset>



                {formData.language.map((language, languageIndex) => {

                    return (
                        <fieldset key={languageIndex}>
                            <legend >language</legend>
                            <div className="form-group">
                                <label className="col-md-3 control-label">code</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control" name="code" value={language["code"]} onChange={(e) => handleChangeLanguage(e, languageIndex)} />
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-md-3 control-label">direction</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control" name="direction" value={language["direction"]} onChange={(e) => handleChangeLanguage(e, languageIndex)} />
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-md-3 control-label">title</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control" name="title" value={language["title"]} onChange={(e) => handleChangeLanguage(e, languageIndex)} />
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-md-3 control-label">header</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control" name="header" value={language["header"]} onChange={(e) => handleChangeLanguage(e, languageIndex)} />
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-md-3 control-label">body</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control" name="body" value={language["body"]} onChange={(e) => handleChangeLanguage(e, languageIndex)} />
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-md-3 control-label">footer</label>
                                <div className="col-md-6">
                                    <input type="text" className="form-control" name="footer" value={language["footer"]} onChange={(e) => handleChangeLanguage(e, languageIndex)} />
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-md-3 control-label">Labels</label>
                                <div className="col-md-6">
                                    <button
                                        className="dt-button btn btn-info"
                                        tabIndex={0} aria-controls="data-table"
                                        title="Add"
                                        type="button"
                                        onClick={(e) => addLanguageButtonsLabel(e, languageIndex)}
                                    >
                                        <span>Add buttons label</span>
                                    </button>
                                </div>
                            </div>

                            {language.button && language.button.map((button, buttonIndex) => {
                                return (
                                    <fieldset key={buttonIndex}>
                                        <div className="form-group">
                                            <label className="col-md-3 control-label">label</label>
                                            <div className="col-md-6">
                                                <input type="text" className="form-control" name="label" value={button["label"]} onChange={(e) => handleButtonLabelChange(e, languageIndex, buttonIndex)} />
                                            </div>
                                        </div>
                                    </fieldset>
                                )
                            })}
                        </fieldset>
                    )
                })}

                <div className="form-actions">
                    <div className="row">
                        <div className="col-md-6">
                            <button
                                className="dt-button btn btn-primary "
                                tabIndex={0} aria-controls="data-table"
                                title="Add"
                                type='submit'
                                // trigger bootstrap validation on first render 
                                disabled={true}
                            >
                                <span>submit</span>
                            </button>

                        </div>
                    </div>
                </div>

                <fieldset>
                    {/* #messages is where the messages are placed inside */}
                    <div className="form-group">
                        <div className="col-md-9 col-md-offset-3">
                            <div id="messages" />
                        </div>
                    </div>
                </fieldset>
            </form>
        </BootstrapValidator>
};

export default AddForm