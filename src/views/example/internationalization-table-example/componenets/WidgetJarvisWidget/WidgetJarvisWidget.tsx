import React from 'react';
import { JarvisWidget, WidgetGrid } from '../../../../../common';

type Props = {
    editbutton?: boolean,
    togglebutton?:boolean
  };


 const WidgetJarvisWidget: React.FC<Props> = ({ children, editbutton=false,togglebutton=false }) => {
    return <WidgetGrid>
        <div className="row">
            <article className="col-sm-12">
                <JarvisWidget id="wid-id-2" editbutton={editbutton} color="blueDark" togglebutton={togglebutton}>
                    <header>
                        <span className="widget-icon">
                            <i className="fa fa-table" />
                        </span>
                    </header>
                    <div>
                        <div className="widget-body no-padding data-table-pop-up" id="myTable">
                            {children}
                        </div>
                    </div>
                </JarvisWidget >
            </article>
        </div>
    </WidgetGrid>;
};

export default WidgetJarvisWidget;