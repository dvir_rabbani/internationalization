type ButtonLabelType = { label: string }

export interface Language {
    code: string,
    direction: string,
    title: string,
    header: string,
    body: string,
    footer: string,
    button?: ButtonLabelType[]
}
export interface FormData {
    page: string,
    language: Language[],


}

export type FormDataProps = {
    data?: FormData,
}