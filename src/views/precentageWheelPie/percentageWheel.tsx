import React from 'react'
import EasyPieChartContainer from '../../common/graphs/inline/components/EasyPieChartContainer';
import './percentageWheel.scss';

interface PercentageWheelProps {
  pieSize: "40" | "60" | "80" | "90" | "100" | "120" | "150";
  modeColor: "greenLight" | "msbityellow" | "redLight";
}

const PercentageWheel: React.FC<PercentageWheelProps> = (props) => {
  const precent = (props.modeColor === "greenLight") ? 90 : (props.modeColor === "msbityellow") ? 50 : 15;
  return (
          <EasyPieChartContainer>
              <ul className="list-inline">
                <li>
                  &nbsp;&nbsp;&nbsp;
                  <div
                    className={`easy-pie-chart txt-color-${props.modeColor} easyPieChart`}
                    data-percent={precent}
                    data-pie-size={props.pieSize}
                    data-pie-track-color={'#444'}
                  >
                    <span className={`percent percent-sign txt-color-${props.modeColor}`}>
                      66
                    </span>
                  </div>
                  &nbsp;&nbsp;&nbsp;
                </li>
              </ul>
          </EasyPieChartContainer>
  )
}
export default PercentageWheel;