import Loadable from "react-loadable";
import { Loading } from "../../common/navigation";

const Profile = Loadable({
  loader: () => import("./components/Profile"),
  loading: Loading
});

export const routes = [
  {
    path: "/profile",
    exact: true,
    component: Profile,
    name: "Profile"
  }
];
