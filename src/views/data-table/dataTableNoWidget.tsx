import React from 'react'
import Datatable from '../../common/tables/components/Datatable';
import './dataTableComponent.scss';

interface IDataTableNoWidgetProps {
    columns: string[];
    thTitle: string[];
    jsonDataTableUrl: string;
    amountPerPage: number;
    tableBordered: boolean;
    actionLinkUrl?: string;
}

const DataTableNoWidget: React.FC<IDataTableNoWidgetProps> = (props) => {

    const createColumnsArray = () => {
        const columnsArray = [];
        for (let i = 0; i < props.columns.length; i++) {
            columnsArray.push({ data: props.columns[i] });
        }
        return columnsArray;
    }

    return (

        <div className="data-table-pop-up">
            <Datatable
                options={{
                    iDisplayLength: props.amountPerPage,
                    ajax: props.jsonDataTableUrl,
                    columns: createColumnsArray(),
                    buttons: [
                        { extend: "print", text: "Print", className: "btn btn-default buttons-print" },
                        { extend: "excel", text: "Excel", className: "btn btn-default buttons-excel" },
                        { extend: "pdf", text: "PDF", className: "btn btn-default buttons-pdf" }
                    ]
                }}
                className={`table table-striped table-hover responsive display nowrap collapsed ${props.tableBordered && 'table-bordered'}`}
                width="80%"
            >
                <thead>
                    <tr>
                        {props.thTitle.map((item, i) => {
                            return (
                                <th key={item + i}>{item}</th>
                            )
                        })}
                    </tr>
                </thead>
            </Datatable>
        </div>

    )
}
export default DataTableNoWidget;