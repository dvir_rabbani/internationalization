import React, { useEffect, useState } from 'react'
import { JarvisWidget, WidgetGrid } from '../../common';
import Datatable from '../../common/tables/components/Datatable';
import './dataTableComponent.scss';
import $ from "jquery";
import { connect } from "react-redux";
import { Msg } from '../../common/i18n';
import PopUpModal from '../modal/modal';
import AddForm from '../example/internationalization-table-example/componenets/addForm/addForm';

interface IDataTableComponentProps {
    // the names of the data from the respone json
    columns: string[];
    // the title each columns that hold your data from the respone json
    thTitle: string[];
    // the url for the "GET" request to get the data for the table
    jsonDataTableUrl: string;
    amountPerPage: number;
    tableBordered: boolean;
    // if there will be a toggle button to the widget to toggle the table
    toggleBtnWidjet: boolean;
    // title of the widget - table name
    title: string;
    // if you want a button other than the default table butttons
    buttonName?: string;
    // addBtnModalId?: string;
    // if you want to add an "Edit/Delete" buttons for each row in the table
    editBtn?: boolean;
    deleteBtn?: boolean;
    // function for the delete button if you added one
    deleteItem?: (id: string | number) => void;
    // setId to the element id when the user click on the edit button of this element
    setId?: React.Dispatch<React.SetStateAction<number | string>>;
    // manipulate data before sending it to the table data 
    dataSrc?: { dataSrc: (data: any) => any }
}

const DataTableComponent: React.FC<IDataTableComponentProps> = (props) => {
    const dataTargetModal = "btnOpenModalExample";
    const [widgetFormData, setWidgetFormData] = useState(null);
    const [toggle, setToggle] = useState<boolean>(false);
    let test = null;


    useEffect(() => {
        // add onClick to the edit item button
        $('#data-table tbody').on('click', '#editTableBtn', function (e) {
            //the attribute data-edit hold the id of the item
            const itemID = $(this)[0].attributes['data-edit'].value;
          console.log("itemID", itemID);
          

            let url = 'http://localhost:3000/internationalization/' + itemID;
            
            fetch(url)
            .then(resp => resp.json())
            .then(data => {
              test = data;  
                setWidgetFormData(data);
                console.log("widgetFormData:", widgetFormData);
                console.log("data: ", data);
           });
           console.log("test: ", test);
           
           
        //    setToggle(!toggle);
          
            // props.setId && props.setId(itemID);
        });
        // add onClick to the delete item button
        $('#data-table tbody').on('click', '#deleteTableBtn', function () {
            //the attribute data-delete hold the id of the item
            const itemID = $(this)[0].attributes['data-delete'].value;

            props.deleteItem && props.deleteItem(itemID);
        });
    }, []);

    const createColumnsArray = () => {
        const columnsArray = [];
        for (let i = 0; i < props.columns.length; i++) {
            // the column of the "id" will be hidden - visible: false
            if (props.columns[i] === "id") {
                columnsArray.push({ data: null, visible: false });
            } else if (props.columns[i] === 'language') {

                columnsArray.push({
                    data: props.columns[i], render: function (data: any, type: string, row: any, meta: any) {
                        let languages = data.join(",");
                        return languages;
                    }
                });
            }

            else if (props.columns[i] === 'update') {
                columnsArray.push({
                    data: props.columns[i], render: function (data: string, type: string, row: any, meta: any) {
                        data = '<a href="/#/referent/update">' + data + '</a>';
                        return data;
                    }
                });
            }
            else columnsArray.push({ data: props.columns[i] });
        }



        // add a column for each row with Edit button if props.editBtn=true
        if (props.editBtn) {


            columnsArray.push({
                data: null, render: function (data: string, type: string, row: any, meta: any) {
                    //the attribute data-edit hold the id of the item
                    data = `<button class="btn btn-default edit-form" data-toggle="modal" data-target=#${dataTargetModal} data-edit=${row.id} id="editTableBtn">Edit</button>`

                    return data;
                }
            });
        }

        // add a column for each row with Delete button if props.deleteBtn=true
        if (props.deleteBtn) {

            columnsArray.push({
                data: null, render: function (data: string, type: string, row: any, meta: any) {
                    //the attribute data-delete hold the id of the item
                    data = '<button data-delete="' + row.id + '" class="btn btn-default" id="deleteTableBtn">Delete</button>';
                    return data;
                }
            });
        }
        return columnsArray;
    }
    const createButton = () => {
        // create print, excel, pdf buttons
        //create a reload button with display-none, and when we want to relaod the table we add trigger 'click' to this button(with the function "reloadDatatable()" in common/consts/functions)
        let dataToggle = "data-toggle"
        let dataTarget = `data-target`
        let buttonsArr: any = [
            // { extend: "print", text: "Print", className: "btn btn-default btn " },
            // { extend: "excel", text: "Excel", className: "btn btn-default buttons-excel" },
            // { extend: "pdf", text: "PDF", className: "btn btn-default buttons-pdf" },
            {
                text: "Add Widget",
                attr: { [dataToggle]: "modal", [dataTarget]: `#${dataTargetModal}` },
                className: "btn btn-primary",
                action: function (e: any, dt: { ajax: { url: (arg0: string) => void; reload: () => void; }; }, node: any, config: any) {
                    e.target.setAttar;
                    dt.ajax.url(props.jsonDataTableUrl);
                    dt.ajax.reload();
                },
            }]
        // add your own button with the props.buttonName if you send it props
        // let btn = { text: props.buttonName, className: "display-none", attr: { title: "Add", id: 'add-button' } }
        // if (props.buttonName) buttonsArr.push(btn);

        return buttonsArr;

    }
    
    return (
        <WidgetGrid>

            {
                widgetFormData
                    ? <PopUpModal
                        btnModalId={dataTargetModal}
                        title={"Widget Form"}
                        children={
                            <>
                                <h2>Add Widget</h2>
                                <AddForm data={widgetFormData} />
                            </>
                        }
                    />
                    : null
            }

            <div className="row">
                <article className="col-sm-12">
                    <JarvisWidget id="wid-id-2" editbutton={false} color="blueDark" togglebutton={props.toggleBtnWidjet}>
                        <header>
                            <span className="widget-icon">
                                <i className="fa fa-table" />
                            </span>
                            <h2><Msg phrase={props.title} /></h2>
                        </header>
                        <div>
                            <div className="widget-body no-padding data-table-pop-up" id="myTable">
                                <Datatable
                                    options={{
                                        iDisplayLength: props.amountPerPage,
                                        ajax: props.jsonDataTableUrl,
                                        columns: createColumnsArray(),
                                        buttons: createButton(),
                                        dataSrc: {
                                            dataSrc: (json) => {
                                                for (var i = 0; i < json.length; i++) {
                                                    if (json[i] && json[i].language) {
                                                        json[i].language = json[i].language.map(language => language.code);

                                                    }

                                                }

                                                return json
                                            }
                                        }


                                    }}
                                    className={`table table-striped table-hover responsive display nowrap collapsed ${props.tableBordered && 'table-bordered'}`}
                                    width="80%"
                                >
                                    <thead>
                                        <tr>
                                            {props.thTitle.map((item: string, i) => {
                                                return (
                                                    <th key={item + i}>
                                                        <Msg phrase={item} />
                                                    </th>
                                                )
                                            })}
                                        </tr>
                                    </thead>
                                </Datatable>
                            </div>
                        </div>
                    </JarvisWidget >
                </article>
            </div>
        </WidgetGrid>
    )
}

const mapStateToProps = (state: { i18n: any; }) => state.i18n;

export default connect(mapStateToProps)(DataTableComponent);